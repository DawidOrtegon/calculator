package Classes;

public class Calculator
{
    public int num1;
    public int num2;

    public int sum(int a, int b)
    {
        return a+b;
    }

    public int subtract(int a, int b)
    {
        return a-b;
    }

    public int multiplication(int a, int b)
    {
        return a*b;
    }

    public int divide(int a, int b)
    {
        return a/b;
    }

    public int power(int a, int b)
    {
        return (int) Math.pow(a,b);
    }

}

