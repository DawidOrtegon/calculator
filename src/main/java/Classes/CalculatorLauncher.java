package Classes;

import java.util.Scanner;

public class CalculatorLauncher
{
    public static void main(String[] args)
    {
        // Select the numbers to make the operation.
        System.out.println("Put the numbers to make the operation");
        Scanner selectNumber = new Scanner(System.in);
        int number1 = selectNumber.nextInt();
        int number2 = selectNumber.nextInt();

        // Select the operation.
        System.out.println("Select the operation");
        Scanner selectOperation = new Scanner(System.in);
        String operation = selectOperation.next();

        Calculator calculator = new Calculator();

        if (operation.equals("+"))
        {
            System.out.println("Result: " + calculator.sum(number1, number2));
        }
        else if(operation.equals("-"))
        {
            System.out.println("Result: " + calculator.subtract(number1, number2));
        }
        else if(operation.equals("*"))
        {
            System.out.println("Result: " + calculator.multiplication(number1, number2));
        }
        else if(operation.equals("/"))
        {
            if(number1 == 0 || number2 == 0)
            {
                System.out.println("Cannot divide by 0, there is not possible");
            }
            else {
                System.out.println("Result: " + calculator.divide(number1, number2));
            }
        }
        else if(operation.equals("ˆ"))
        {
            System.out.println("Result: " + calculator.power(number1, number2));
        }
    }

}